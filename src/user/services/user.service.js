(function(angular){
    'use strict';

    angular.module('gitlabKBApp.user').factory('UserService', 
        [
            '$http', 
            '$q', 
            function ($http, $q) {
                return {
                    users: {},
                    list: function (boardId) {
                        var _this = this;
                        if (_.isEmpty(_this.users[boardId])) {
                            _this.users[boardId] = $http.get('api/users', {params: {project_id: boardId}}).then(function (result) {
                                _this.users[boardId] = result.data;
                                return _this.users[boardId];
                            })
                        }
                        return $q.when(_this.users[boardId]);
                    }
                };
            }
        ]
    );
})(window.angular);


