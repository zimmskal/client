(function (angular) {
    'use strict';
    angular.module('gitlabKBApp.board').factory('BoardService',
        [
            '$http',
            '$q',
            '$sce',
            'LabelService',
            'WebsocketService',
            'Board',
            function ($http, $q, $sce, LabelService, WebsocketService, Board) {
                var service = {
                    boards: {},
                    boardIdIndex: {},
                    boardsList: {},
                    get: function (path) {
                        if (_.isEmpty(this.boards[path])) {
                            this.boards[path] = $q.all([
                                LabelService.list(path),
                                $http.get('/api/cards', {params: {project_id: path}}),
                                $http.get('/api/board', {params: {project_id: path}})
                            ]).then(function (results) {
                                var board = new Board(results[0], results[1].data.data, results[2].data.data);
                                this.boards[path] = board;
                                this.boardIdIndex[board.project.id] = board.project.path_with_namespace;

                                return this.boards[path];
                            }.bind(this));
                        }

                        return $q.when(this.boards[path]);

                    },
                    getBoardById: function(id) {
                        var path = this.boardIdIndex[id];
                        return this.get(path);
                    },
                    getCard: function (boardId, cardId) {
                        return this.get(boardId).then(function (result) {
                            for (var stageName in result.stages) {
                                var stage = result.stages[stageName];
                                var card = _.find(stage.cards, function (card) {
                                    return card.iid == cardId
                                });
                                if (card !== undefined) {
                                    return card;
                                }
                            }
                        });
                    },
                    createCard: function (data) {
                        return $http.post('/api/card', data).then(function (newCard) {
                        });
                    },
                    updateCard: function (card) {
                        return $http.put('/api/card', {
                            issue_id: card.id,
                            project_id: card.project_id,
                            assignee_id: card.assignee_id,
                            milestone_id: card.milestone_id,
                            title: card.title,
                            labels: card.labels.join(', '),
                            todo: card.todo,
                            description: card.description
                        }).then(function (result) {
                        });
                    },
                    removeCard: function (card) {
                        var _this = this;
                        return $http.delete('/api/card', {
                            data: {
                                project_id: card.project_id,
                                issue_id: card.id,
                                closed: 1
                            },
                            headers: {'Content-Type': 'application/json'}
                        }).then(function (result) {
                            return _this.removeCardFromBoard(card);
                        });
                    },
                    getBoards: function () {
                        var _this = this;
                        if (_.isEmpty(_this.boardsList)) {
                            _this.boardsList = $http.get('/api/boards').then(function (result) {
                                _this.boardsList = _.indexBy(result.data.data, 'id');
                                return _this.boardsList;
                            }, function (result) {
                                _this.boardsList = {};
                                return $q.reject(result);
                            });
                        }

                        return $q.when(_this.boardsList);
                    },
                    addCardToBoard: function (card) {
                        var _this = this;
                        return _this.getBoardById(card.project_id).then(function (board) {
                            card.viewLabels = [];
                            for (var i = 0; i < card.labels.length; i++) {
                                var viewLabel = board.viewLabels[card.labels[i]];
                                if (viewLabel !== undefined) {
                                    card.viewLabels.push(viewLabel);
                                }
                            }
                            var stageLabel = _.intersection(board.labels, card.labels);
                            var found = false;
                            for (var stageName in board.stages) {
                                if (stageLabel == board.stages[stageName].label) {
                                    found = true;
                                    board.stages[stageName].cards.unshift(card);
                                }
                            }
                            if (!found) {
                                board.stages[0].cards.unshift(card);
                            }
                        });
                    },
                    removeCardFromBoard: function (card) {
                        var _this = this;
                        return _this.getBoardById(card.project_id).then(function (result) {
                            for (var stageName in result.stages) {
                                for (var cardIndex in result.stages[stageName].cards) {
                                    var cardToCheck = result.stages[stageName].cards[cardIndex];

                                    if (cardToCheck.id == card.id) {
                                        result.stages[stageName].cards.splice(cardIndex, 1);
                                    }
                                }
                            }
                        });
                    },
                    updateCardOnBoard: function (card) {
                        var _this = this;
                        return _this.getBoardById(card.project_id).then(function (board) {
                            _this.getCard(board.project.path_with_namespace, card.iid).then(function (oldCard) {
                                for (var fieldName in card) {
                                    oldCard[fieldName] = card[fieldName];
                                }

                                oldCard.viewLabels = [];
                                for (var i = 0; i < oldCard.labels.length; i++) {
                                    var viewLabel = board.viewLabels[oldCard.labels[i]];
                                    if (viewLabel !== undefined) {
                                        oldCard.viewLabels.push(viewLabel);
                                    }
                                }

                                var stageLabel = _.intersection(board.labels, oldCard.labels);
                                var newStageLabel = _.intersection(board.labels, card.labels);
                                var found = false;

                                for (var stageName in board.stages) {
                                    var stage = board.stages[stageName];
                                    for (var cardIndex in stage.cards) {
                                        if (oldCard.id === stage.cards[cardIndex].id) {
                                            stage.cards.splice(cardIndex, 1);
                                        }
                                    }
                                }

                                for (var stageName in board.stages) {
                                    if (newStageLabel == board.stages[stageName].label) {
                                        found = true;
                                        board.stages[stageName].cards.unshift(oldCard);
                                        board.stages[stageName].cards = _.sortBy(board.stages[stageName].cards, function (c) {
                                            return c.iid * -1
                                        });
                                    }
                                }

                                if (!found) {
                                    board.stages[0].cards.unshift(card);
                                }
                            });
                        });
                    }
                };

                WebsocketService.on('card.create', function (data) {
                    return service.addCardToBoard(data);
                });

                WebsocketService.on('card.delete', function (data) {
                    return service.removeCardFromBoard(data);
                });

                WebsocketService.on('card.update', function (data) {
                    return service.updateCardOnBoard(data);
                });

                return service;
            }
        ]
    );
})(window.angular);

