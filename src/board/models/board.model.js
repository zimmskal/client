(function(angular) {
    'use strict';

    angular.module('gitlabKBApp.board').factory('Board', 
        [
            'Stage',
            function(Stage) {
                function Board(labels, issues, project) {
                    this.stages = []; 
                    this.viewLabels = [];
                    this.labels = [];
                    this.project = project;
                    
                    var pattern = /KB\[stage\]\[\d\]\[(.*)\]/;
                    var stages = _.sortBy(_.filter(labels, function(label) {
                        return pattern.test(label.name);
                    }), "name");

                    this.viewLabels = _.indexBy(_.difference(labels, stages), 'name');
                    this.labels = _.pluck(stages, 'name');
                    
                    issues = _.map(issues, function(issue, key) {
                        issue.viewLabels = [];

                        if (!_.isEmpty(issue.labels)) {
                            var labels = issue.labels;
                            for (var i = 0; i < labels.length; i++) {
                                var label = this.viewLabels[labels[i]];
                                if (label !== undefined) {
                                    issue.viewLabels.push(label);
                                }
                            }
                        }

                        return issue;
                    }, this);

                    for (var i in stages) {
                        this.stages.push(new Stage(
                            stages[i].name,
                            [],
                            stages[i].name.match(pattern)[1]
                        ));
                    }

                    var cards = _.groupBy(issues, function(issue) {
                        var stage = _.intersection(this.labels, issue.labels);
                        if (stage.length === 0) {
                            return this.labels[0];
                        } else {
                            return stage[0];
                        }
                    }, this);

                    for (var stage in this.stages) {
                        this.stages[stage].cards = cards[this.stages[stage].label];
                    }

                }

                return Board;
            }
        ]
    );
})(window.angular);
