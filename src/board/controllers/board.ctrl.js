(function (angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller('BoardController',
        [
            '$scope',
            '$http',
            '$stateParams',
            'BoardService',
            '$state',
            '$window',
            'WebsocketService',
            function ($scope, $http, $stateParams, BoardService, $state, $window, WebsocketService) {
                $window.scrollTo(0, 0);
                $scope.filterCriteria = {};

                if ($stateParams.assignee !== undefined) {
                    $scope.filterCriteria.assignee = {
                        username: $stateParams.assignee.replace(/@/, '')
                    };
                }

                if ($stateParams.milestone !== undefined) {
                    $scope.filterCriteria.milestone = {
                        data: {
                            title: $stateParams.milestone.replace(/\^/, '')
                        }
                    };
                }

                BoardService.get($stateParams.project_path).then(function (board) {
                    if (_.isEmpty(board.stages)) {
                        $state.go('board.import', {project_path: board.project.path_with_namespace});
                    }
                    $scope.board = board;

                    WebsocketService.emit('board.view', {board: board.project.id.toString()});
                });

                $scope.state = $state;

                $scope.dragControlListeners = {
                    itemMoved: function (event) {
                        var card = event.source.itemScope.card;
                        var newLabel = event.dest.sortableScope.$parent.stage.label;

                        var pattern = /KB\[stage\]\[\d\]\[(.*)\]/;
                        card.labels = _.filter(card.labels, function (label) {
                            return !pattern.test(label);
                        });

                        card.labels.push(newLabel);

                        $http.put('/api/card', {
                            project_id: card.project_id,
                            issue_id: card.id,
                            title: card.title,
                            labels: card.labels.join(', ')
                        }).success(function (data) {

                        });
                    },
                    orderChanged: function (event) {
                    },
                    containment: '#board'
                };

                $scope.remove = function (card) {
                    BoardService.removeCard(card).then(function (result) {
                    });
                };
            }
        ]
    );
})(window.angular);
