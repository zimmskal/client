(function (angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller('TopBarController',
        [
            '$scope',
            '$state',
            '$stateParams',
            'BoardService',
            'AuthService',
            '$window',
            'UserService',
            'MilestoneService',
            function ($scope, $state, $stateParams, BoardService, AuthService, $window, UserService, MilestoneService) {
                var milestoneRegex = /\^/;
                var usersRegex = /@/;

                if ($stateParams.project_path !== undefined) {
                    BoardService.get($stateParams.project_path).then(function (board) {
                        $scope.project = board.project;
                    });
                }

                $scope.logout = function () {
                    AuthService.logout();
                    $window.location.reload();
                };

                $scope.filterval = $stateParams.assignee
                                    || $stateParams.milestone
                                    || '';

                $scope.filter = function () {
                    var stateParams = {
                        project_path: $stateParams.project_path,
                        milestone: '',
                        assignee: ''
                    };

                    if (milestoneRegex.test($scope.filterval)) {
                        stateParams.milestone = $scope.filterval;
                    }

                    if (usersRegex.test($scope.filterval)) {
                        stateParams.assignee = $scope.filterval;
                    }

                    $state.go('board.cards', stateParams);
                };

                $scope.getFilter = function (val) {
                    if (usersRegex.test(val)) {
                        return UserService.list($stateParams.project_path).then(function (users) {
                            var u = _.map(users, function (user) {
                                return "@" + user.username;
                            });

                            return _.filter(u, function (name) {
                                return name.toLowerCase().indexOf(val.toLowerCase()) !== -1;
                            });

                        });
                    }

                    if (milestoneRegex.test(val)) {
                        return MilestoneService.list($stateParams.project_path).then(function (milestones) {
                            var m = _.map(milestones, function (milestone) {
                                return "^" + milestone.title;
                            });

                            return m;
                        });
                    }

                    return [];
                };

                $scope.state = $state;
            }
        ]
    );
})(window.angular);

